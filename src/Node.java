import java.util.Arrays;
import java.util.Stack;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      // TODO!!! Your constructor here
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }

   public static void checkinput(String s){
      if (s.contains(",,"))throw new RuntimeException("Two commas:"+s);
      if (s.contains("\t"))throw new RuntimeException("Contain Spaces:"+s);
      if (s.contains("\s"))throw new RuntimeException("Spaces:"+s);
      if (s.contains("()"))throw new RuntimeException("Only Brackets:"+s);
      if (s.contains("((") && s.contains("))"))throw new RuntimeException("Multiple Layered Brackets:"+s);
      if (s.contains(",") && !(s.contains("(") && s.contains(")")))throw new RuntimeException("Non need comma"+s);
   }
   
   public static Node parsePostfix (String s) {
      checkinput(s);
      String[] strArr = s.split("");
      System.out.println("strArr:"+ Arrays.toString(strArr));
      Stack<Node> stack = new Stack();
      Node node = new Node(null, null, null);
      boolean replacingRoot = false;

      int i=0;
      while(i < strArr.length){
         switch (strArr[i]) {
            case "(":
               if (replacingRoot) throw new RuntimeException("Cannot replace the root");
               if (strArr[i + 1].equals(",")) throw new RuntimeException("Error input [(,] ");

               //Save the node ==> the stack
               //Set the node as a Child, because it's inside of brackets
               stack.push(node);
               node.firstChild = new Node(null, null, null);
               node = node.firstChild;
               break;
            case ")":
               //Pop the node from stack
               node = stack.pop();
               //Stack is empty => replacing root
               if (stack.size() == 0) replacingRoot = true;
               break;
            case ",":
               if (replacingRoot) throw new RuntimeException("Trying to replace root");

               //[,] means Sibling
               node.nextSibling = new Node(null, null, null);
               node = node.nextSibling;
               break;
            default:
               //Store each chars to the name
               if (node.name == null) {
                  node.name = strArr[i];
               } else {
                  node.name += strArr[i];
               }
               break;
         }
         i++;
      }

      return node;
   }

   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();

      //Root ==> Child ==> Sibling
      sb.append(this.name);

      if (this.firstChild != null) {
         sb.append("(");
         sb.append(this.firstChild.leftParentheticRepresentation());
         sb.append(")");
      }

      if (this.nextSibling != null) {
         sb.append(",");
         sb.append(this.nextSibling.leftParentheticRepresentation());
      }
      return sb.toString(); // TODO!!! return the string without spaces
   }

   public static void main (String[] param) {
//      String s = "((C,(E)D)B,F)A";
      String s = "(((512,1)-,4)*,(-6,3)/)+";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}

//Reference from https://git.wut.ee/i231/home5/src/branch/master/src/Node.java
